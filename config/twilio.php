<?php

return [

    'twilio' => [

        'default' => 'twilio',

        'connections' => [

            'twilio' => [

                /*
                |--------------------------------------------------------------------------
                | SID
                |--------------------------------------------------------------------------
                |
                | Your Twilio Account SID #
                |
                */

                // 'sid' => getenv('TWILIO_SID') ?: 'AC97c0e774914a605645b719a2d3518215',
                'sid' => getenv('TWILIO_SID') ?: 'AC844679a2bee1dca520f626d310d7a78a', //test

                /*
                |--------------------------------------------------------------------------
                | Access Token
                |--------------------------------------------------------------------------
                |
                | Access token that can be found in your Twilio dashboard
                |
                */

                // 'token' => getenv('TWILIO_TOKEN') ?: '091ba756c96f2c46c26aaa03a0a39353',
                'token' => getenv('TWILIO_TOKEN') ?: 'ceefe513b58ce487806adcf7fecd86d5',     //test

                /*
                |--------------------------------------------------------------------------
                | From Number
                |--------------------------------------------------------------------------
                |
                | The Phone number registered with Twilio that your SMS & Calls will come from
                |
                */

                // 'from' => getenv('TWILIO_FROM') ?: '(202) 831-8394 ',
                'from' => getenv('TWILIO_FROM') ?: '+966508772979',

                /*
                |--------------------------------------------------------------------------
                | Verify Twilio's SSL Certificates
                |--------------------------------------------------------------------------
                |
                | Allows the client to bypass verifying Twilio's SSL certificates.
                | It is STRONGLY advised to leave this set to true for production environments.
                |
                */

                // 'ssl_verify' => true,
                'ssl_verify' => false,
            ],
        ],
    ],
];
