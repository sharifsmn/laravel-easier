@extends('layout')

@section('content')
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-10 col-md-offset-1">
            <!-- general form elements -->
            <div class="box box-primary">            
                <div class="box-header with-border">
                    <h3 class="box-title">Quote Information</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                {{-- errors message --}}
            	@if (count($errors) > 0)
            	    <div class="alert alert-danger">
            	        <ul>
            	            @foreach ($errors->all() as $error)
            	                <li>{{ $error }}</li>
            	            @endforeach
            	        </ul>
            	    </div>
            	@endif
                <form role="form" action="{{ url('quote/add') }}" method="POST" enctype="multipart/form-data">
                	{{ csrf_field() }}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="customer">Customer</label>
                            <select class="form-control" id="customer" name="customer">
                            @foreach($customers as $customer)   
                                <option value="{{$customer->id}}">{{$customer->name}}</option>
                            @endforeach    
                            </select>
                        </div>
                        <div class="box-body">
                        <div class="form-group">
                            <label for="grocery">Grocery</label>
                            <select class="form-control" id="grocery" name="grocery">
                            @foreach($grocerys as $grocery)   
                                <option value="{{$grocery->id}}">{{$grocery->name}}</option>
                            @endforeach    
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="lat">Lat</label>
                            <input type="text" class="form-control" id="lat" name="lat" placeholder="Enter lat">
                        </div>
                        <div class="form-group">
                            <label for="lon">lon</label>
                            <input type="text" class="form-control" id="lon" name="lon" placeholder="Enter lon">
                        </div>
                        <div class="form-group">
                            <label for="status">Status</label>
                            <select class="form-control" id="status" name="status">
                                <option value="active">Active</option>
                                <option value="pending">Pending</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="lon">comment</label>
                            <input type="text" class="form-control" id="comment" name="comment" placeholder="Enter comment">
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Add Product</button>
                    </div>
                </form>
            </div>
        </div>    
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
<!-- /.box -->
@endsection