@extends('layout')

@section('content')
<section class="content">
    <div class="row">
        <!-- left column -->
            <!-- general form elements -->
            <div class="box box-primary">            
                <div class="box-header with-border">
                    <h3 class="box-title">Customer Information Update</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                @if (Session::has('activity_msg_success'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{ Session::get('activity_msg_success') }}
                    </div>
                @endif
                {{-- errors message --}}
            	@if (count($errors) > 0)
            	    <div class="alert alert-danger">
            	        <ul>
            	            @foreach ($errors->all() as $error)
            	                <li>{{ $error }}</li>
            	            @endforeach
            	        </ul>
            	    </div>
            	@endif
                <form role="form" action="{{ url('customer/'.$customer->id.'/update') }}" 		method="POST">
                	{{ csrf_field() }}
                	{{ method_field('patch') }}
                    <div class="box-body">
        				<div class=" col-md-6">
	                        <div class="form-group">
	                            <label for="name">Name</label>
	                            <input type="text" class="form-control" id="name" name="name" value="{{ $customer->name }}">
	                        </div>
	                        <div class="form-group">
	                            <label for="name">UserName</label>
	                            <input type="text" class="form-control" id="name" name="u_name" value="{{ $customer->username }}" disabled="">
	                        </div>
	                        <div class="form-group">
	                            <label for="mobile">Mobile Number</label>
	                            <input type="number" class="form-control" id="mobile" name="mobile" value="{{ $customer->mobile }}">
	                        </div>
	                        <div class="form-group">
	                            <label for="exampleInputEmail1">Email address</label>
	                            <input type="email" class="form-control" name="email" id="exampleInputEmail1" value="{{ $customer->email }}">
	                        </div>
	                        <div class="form-group">
	                            <label for="exampleInputPassword1">Password</label>
	                            <input type="password" class="form-control" id="exampleInputPassword1" name="password" value="{{ $customer->password }}">
	                        </div>
	                        <div class="form-group">
	                            <label for="country">Status</label>
	                            <select name="status" class="form-control" id="country">                            
									<option value="active" {{ $customer->status =="active" ? 'selected ="selected"':''}}>Active</option>
									<option value="pending" {{ $customer->status =="pending" ? 'selected ="selected"':'' }}>Pending</option>
	                            </select>
	                        </div>
	                    </div>
	                    <div class="col-md-6">
	                        <div class="form-group">
	                            <label for="city">City</label>
	                            <input type="text" name="city" class="form-control" id="city" value="{{ $customer->city}}" >
	                        </div>
	                        <div class="form-group">
	                            <label for="district">District</label>
	                            <input type="text" name="district" class="form-control" id="district" value="{{ $customer->district}}" >
	                        </div>
	                        <div class="form-group">
	                            <label for="country">Country</label>
	                            <select name="country" class="form-control" id="country">
	                            	@foreach($countrys as $country)
										<option value="{{ $country->code }}" {{ $customer->country == $country->code ?'selected="selected"':''}}>{{ $country->name }}</option>
									@endforeach	
	                            </select>
	                        </div>
	                        <div class="form-group">
	                            <label for="address"> Address</label>
	                            <input type="text" class="form-control" id="addres" name="addres" value="{{ $customer->address_1}}">
	                        </div>

	                        <div class="form-group">
	                            <label for="lat">Lat</label>
	                            <input type="number" class="form-control" id="lat" name="lat" value="{{ $customer->lat}}">
	                        </div>
	                        <div class="form-group">
	                            <label for="lon">Lon</label>
	                            <input type="number" class="form-control" id="lon" name="lon" value="{{ $customer->lon}}">
	                        </div>                  	
	                    </div>    
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer" style="text-align:center">
                        <button type="submit" class="btn btn-primary">Update Customer</button>
                    </div>
                </form>
            </div>
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
<!-- /.box -->
@endsection