@if (Auth::check())
<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="http://placehold.it/160x160/00a65a/ffffff/&text={{ Auth::user()->name[0] }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">{{ trans('backpack::base.administration') }}</li>
            <!-- ================================================ -->
            <!-- ==== Recommended place for admin menu items ==== -->
            <!-- ================================================ -->
            <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
            <li class="treeview">
                <a href="#"><i class="fa fa-cubes"></i><span>Customers</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('customer/') }}"><i class="fa fa-circle-o"></i> <span>All Customers</span></a></li>
                    <li><a href="{{ url('customer/create') }}"><i class="fa fa-circle-o"></i> <span>Add New Customers</span></a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class="fa fa-cubes"></i><span>Groceries</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('grocerie/') }}"><i class="fa fa-circle-o"></i> <span>All Groceries</span></a></li>
                    <li><a href="{{ url('grocerie/create') }}"><i class="fa fa-circle-o"></i> <span>Add New Groceries</span></a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class="fa fa-cubes"></i><span>Grocery Attributes</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('groceryattr/') }}"><i class="fa fa-circle-o"></i> <span>All Grocery Attributes</span></a></li>
                    <li><a href="{{ url('groceryattr/create') }}"><i class="fa fa-circle-o"></i> <span>Add New Grocery Attributes</span></a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class="fa fa-cubes"></i><span>Products</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('product/') }}"><i class="fa fa-circle-o"></i> <span>All Products</span></a></li>
                    <li><a href="{{ url('product/create') }}"><i class="fa fa-circle-o"></i> <span>Add New Products</span></a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class="fa fa-cubes"></i><span>Quotes</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('quote/') }}"><i class="fa fa-circle-o"></i> <span>All Quotes</span></a></li>
                    <li><a href="{{ url('quote/create') }}"><i class="fa fa-circle-o"></i> <span>Add New Quotes</span></a></li>
                    <li><a href="{{ url('quoteitem/') }}"><i class="fa fa-circle-o"></i> <span>All Quote Item</span></a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class="fa fa-cubes"></i><span>Orders</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('order/') }}"><i class="fa fa-circle-o"></i> <span>All Order</span></a></li>
                    <li><a href="{{ url('order/create') }}"><i class="fa fa-circle-o"></i> <span>Add New Orders</span></a></li>
                    <li><a href="{{ url('orderitem/') }}"><i class="fa fa-circle-o"></i> <span>All Order Items</span></a></li>
                </ul>
            </li>
            <!-- ======================================= -->
            <li class="header">{{ trans('backpack::base.user') }}</li>
            <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/logout') }}"><i class="fa fa-sign-out"></i> <span>{{ trans('backpack::base.logout') }}</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
@endif