@extends('layout')

@section('content')
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-10 col-md-offset-1">
            <!-- general form elements -->
            <div class="box box-primary">            
                <div class="box-header with-border">
                    <h3 class="box-title">Order Information</h3>
                </div>
            	@if (count($errors) > 0)
            	    <div class="alert alert-danger">
            	        <ul>
            	            @foreach ($errors->all() as $error)
            	                <li>{{ $error }}</li>
            	            @endforeach
            	        </ul>
            	    </div>
            	@endif
                <form role="form" action="{{ url('orderitem/add') }}" method="POST" enctype="multipart/form-data">
                	{{ csrf_field() }}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="customer">Order Name</label>
                            <select class="form-control" id="order" name="order">
                            @foreach($orders as $orders)   
                                <option value="{{$orders->id}}">{{$orders->id}}</option>
                            @endforeach    
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="lon">Name</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Enter name">
                        </div>
                        <div class="form-group">
                            <label for="lon">Qty</label>
                            <input type="text" class="form-control" id="qty" name="qty" placeholder="Enter qty">
                        </div>
                        <div class="form-group">
                            <label for="lon">Price</label>
                            <input type="number" class="form-control" id="order" name="price" placeholder="Enter price">
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Add order</button>
                    </div>
                </form>
            </div>
        </div>    
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
<!-- /.box -->
@endsection