@extends('layout')

@section('content')
<div class="Section___section Section___type_d Section___padding">
    <div class="login-section">
        <h2 class="Heading___heading Heading___xsSizeXlg Heading___inline spacing___mgnBottom0 spacing___mgnRightXs spacing___mgnTop0">Log In</h2>
        <h4 class="Heading___heading Heading___xsSizeSm Heading___inline Heading___light spacing___mgn0"><a href="{{ url('/register') }}">Don't have an account?</a></h4>
        <form class="LoginForm___form" method="POST" action="{{ url('/login') }}">
            {{ csrf_field() }}
            <div class="spacing___mgnBottomXs spacing___mgnTopXs{{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="text" class="Input___input FormFields___formField Input___md FormFields___md" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus >
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="{{ $errors->has('password') ? ' has-error' : '' }}">
                <input class="Input___input FormFields___formField Input___md FormFields___md" placeholder="Password" name="password" type="password" required>


                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif

            </div>
            <div class="spacing___mgnBottomXs spacing___mgnTopXs"><button class="Button___md Button___primary Button___button Button___block" type="submit">Login</button></div>
        </form>
        <div class="FinePrint___finePrint spacing___padTopXs">
            <a href="{{ url('/password/reset') }}">Forgot your password?</a>
        </div>
    </div>
</div>
@endsection
