@extends('layout')

@section('content')
<section class="content-header">
    <h1>
        Customer Tables
        <small>advanced tables</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="create pull-right" style="padding:10px">
        <a href="{{ url('quoteitem/create')}}"><button class="btn btn-success">Create New Quote </button></a>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Quote Item Data Table</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example2" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Qut</th>
                                <th>Price</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($quoteitems as $quoteitem)
                                <tr>
                                    <td>{{ $quoteitem->id }}</td>
                                    <td>{{ $quoteitem->name }}</td>
                                    <td>{{ $quoteitem->qty }}</td>
                                    <td>{{ $quoteitem->price }}</td>
                                    <td>
                                        <a href="{{ url('quoteitem/'.$quoteitem->id.'/edit')}}" style="padding-right:10px"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                        <a href="" style="color:red"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
@endsection