@extends('layout')

@section('content')
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-10 col-md-offset-1">
            <!-- general form elements -->
            <div class="box box-primary">            
                <div class="box-header with-border">
                    <h3 class="box-title">Quote Item Information</h3>
                </div>
            	@if (count($errors) > 0)
            	    <div class="alert alert-danger">
            	        <ul>
            	            @foreach ($errors->all() as $error)
            	                <li>{{ $error }}</li>
            	            @endforeach
            	        </ul>
            	    </div>
            	@endif
                <form role="form" action="{{ url('quoteitem/add') }}" method="POST" enctype="multipart/form-data">
                	{{ csrf_field() }}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="quote">Quote Name</label>
                            <select class="form-control" id="quote" name="quote">
                            @foreach($quotes as $quotes)   
                                <option value="{{$quotes->id}}">{{$quotes->id}}</option>
                            @endforeach    
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="lon">Name</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Enter name">
                        </div>
                        <div class="form-group">
                            <label for="lon">Qty</label>
                            <input type="text" class="form-control" id="qty" name="qty" placeholder="Enter qty">
                        </div>
                        <div class="form-group">
                            <label for="lon">Price</label>
                            <input type="number" class="form-control" id="order" name="price" placeholder="Enter price">
                        </div>
                        <div class="form-group">
                            <label for="status">Quote Name</label>
                            <select class="form-control" id="status" name="status">    <option value="active">Active</option>
                                <option value="pending">Pending</option>
                            </select>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Add Quote Item</button>
                    </div>
                </form>
            </div>
        </div>    
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
<!-- /.box -->
@endsection