@extends('layout')

@section('content')
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-10 col-md-offset-1">
            <!-- general form elements -->
            <div class="box box-primary">            
                <div class="box-header with-border">
                    <h3 class="box-title">Grocery Attributes Information</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                {{-- errors message --}}
                @if(Session::has('status'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                        <p>{{Session::get('status')}}</p>
                    </div>
                @endif
            	@if (count($errors) > 0)
            	    <div class="alert alert-danger">
            	        <ul>
            	            @foreach ($errors->all() as $error)
            	                <li>{{ $error }}</li>
            	            @endforeach
            	        </ul>
            	    </div>
            	@endif
                <form role="form" action="{{ url('groceryattr/'.$groceryattributes->id.'/update') }}" method="POST" enctype="multipart/form-data">
                	{{ csrf_field() }}
                    {{method_field('patch')}}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="code">Code</label>
                            <input type="text" class="form-control" id="code" name="code" value ="{{$groceryattributes->code}}">
                        </div>
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" value ="{{$groceryattributes->name}}">
                        </div>
                        <div class="form-group">
                            <label for="image">Image</label>
                            <p class="btn-success">{{ $groceryattributes->image }}</p>
                            <input type="file" class="form-control" id="image" name="image">
                        </div>
                        <div class="form-group">
                            <label for="country">Type</label>
                            <select name="type" class="form-control" id="country">
								<option value="attribute" {{$groceryattributes->type == "attribute"?'selected="selected"':''}}>Attribute</option>
								<option value="timings" {{$groceryattributes->type == "timings"?'selected="selected"':''}}>Timings</option>
								<option value="payment" {{$groceryattributes->type == "payment"?'selected="selected"':''}}>Payment</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="open">Open At</label>
                            <input type="time" name="open" class="form-control" id="open" value ="{{$groceryattributes->open_at}}" >
                        </div>
                        <div class="form-group">
                            <label for="close">Close At</label>
                            <input type="time" name="close" class="form-control" id="close" value ="{{$groceryattributes->close_at}}" >
                        </div>
                        <div class="form-group">
                            <label for="order">Order</label>
                            <input type="number" class="form-control" id="order" name="order" value ="{{$groceryattributes->order}}">
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Update Groceryattribute</button>
                    </div>
                </form>
            </div>
        </div>    
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
<!-- /.box -->
@endsection