@extends('layout')

@section('content')
<section class="content-header">
    <h1>
        Customer Tables
        <small>advanced tables</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Customer Data Table</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Image Path</th>
                                <th>Open At</th>
                                <th>Close At</th>
                                <th>Order</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($grocerys as $grocery)
                                <tr>
                                    <td>{{ $grocery->id }}</td>
                                    <td>{{ $grocery->name }}</td>
                                    <td>{{ $grocery->image }}</td>
                                    <td>{{ $grocery->open_at }}</td>
                                    <td>{{ $grocery->close_at }}</td>
                                    <td>{{ $grocery->order }}</td>
                                    <td>
                                        <a href="{{ url('groceryattr/'.$grocery->id.'/edit') }}" style="padding-right:10px"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                        <a href="" style="color:red"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                {{-- <tr>
                                    <img src="{{ asset("storage/app/upload/image/".$grocery->image) }}" alt="">
                                </tr> --}}
                            @endforeach
                    </table>
                    {{ $grocerys->links() }}
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
@endsection