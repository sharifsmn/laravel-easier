@extends('layout')

@section('content')
<section class="content">
    <div class="row">
        <!-- left column -->
            <!-- general form elements -->
            <div class="box box-primary">            
                <div class="box-header with-border">
                    <h3 class="box-title">Grocery Information</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                {{-- errors message --}}
            	@if (count($errors) > 0)
            	    <div class="alert alert-danger">
            	    	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
            	        <ul>
            	            @foreach ($errors->all() as $error)
            	                <li>{{ $error }}</li>
            	            @endforeach
            	        </ul>
            	    </div>
            	@endif
                <form role="form" action="{{ url('grocerie/add') }}" method="POST" enctype="multipart/form-data">
                	{{ csrf_field() }}
                    <div class="box-body">
        				<div class=" col-md-6">
	                        <div class="form-group">
	                            <label for="name">Name</label>
	                            <input type="text" class="form-control" id="name" name="name" placeholder="Enter name">
	                        </div>
	                        <div class="form-group">
	                            <label for="logo">Logo</label>
	                            <input type="file" class="form-control" id="logo" name="logo">
	                        </div>
	                        <div class="form-group">
	                            <label for="mobile">Mobile Number</label>
	                            <input type="number" class="form-control" id="mobile" name="mobile" placeholder="Enter Mobile number">
	                        </div>

	                        <div class="form-group">
	                            <label for="rnumber">Reg Number</label>
	                            <input type="text" class="form-control" id="rnumber" name="r_number" placeholder="Enter Reg number">
	                        </div>
	                        <div class="form-group">
	                            <label for="sname">Staff Name</label>
	                            <input type="text" class="form-control" id="sname" name="s_name" placeholder="Enter staff name">
	                        </div>    
	                        <div class="form-group">
	                            <label for="exampleInputEmail1">Email address</label>
	                            <input type="email" class="form-control" name="email" id="exampleInputEmail1" placeholder="Enter email">
	                        </div>
	                        <div class="form-group">
	                            <label for="exampleInputPassword1">Password</label>
	                            <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Password">
	                        </div>
	                        <div class="form-group">
	                            <label for="country">Status</label>
	                            <select name="status" class="form-control" id="country">                            
									<option value="active">Active</option>
									<option value="pending">Pending</option>
	                            </select>
	                        </div>	                       
	                        <div class="form-group">
	                            <label for="lat">Lat</label>
	                            <input type="number" class="form-control" id="lat" name="lat" placeholder="Enter lat number">
	                        </div>
	                    </div>
	                    <div class="col-md-6">
	                        <div class="form-group">
	                            <label for="holder">Iban Holder</label>
	                            <input type="text" name="holder" class="form-control" id="holder" placeholder="Ibn Holder" >
	                        </div>
	                        <div class="form-group">
	                            <label for="iban">Iban</label>
	                            <input type="text" name="iban" class="form-control" id="iban" placeholder="Enter iban" >
	                        </div>
	                        <div class="form-group">
	                            <label for="order">Min Order</label>
	                            <input type="number" name="m_order" class="form-control" id="order" placeholder="Enter order" >
	                        </div>
	                        <div class="form-group">
	                            <label for="description">Description</label>
	                            <input type="text" name="description" class="form-control" id="description" placeholder="Enter description" >
	                        </div>
	                        <div class="form-group">
	                            <label for="city">City</label>
	                            <input type="text" name="city" class="form-control" id="city" placeholder="Enter city" >
	                        </div>
	                        <div class="form-group">
	                            <label for="district">District</label>
	                            <input type="text" name="district" class="form-control" id="district" placeholder="Enter District" >
	                        </div>
	                        <div class="form-group">
	                            <label for="country">Country</label>
	                            <select name="country" class="form-control" id="country">
	                            	@foreach($countrys as $country)
										<option value="{{ $country->code }}">{{ $country->name }}</option>
									@endforeach	
	                            </select>
	                        </div>
	                        <div class="form-group">
	                            <label for="address"> Address</label>
	                            <input type="text" class="form-control" id="addres" name="addres" placeholder="Enter Address Information">
	                        </div>

	                        <div class="form-group">
	                            <label for="lon">Lon</label>
	                            <input type="number" class="form-control" id="lon" name="lon" placeholder="Enter lon number">
	                        </div>                  	
	                    </div>
	                    <div class="checkbox">
		                    @foreach($attrs as $attr)
								<label class="checkbox-inline col-md-5" style="padding:18px">
								  	<input type="checkbox" id="inlineCheckbox1" name="attr[{{$attr->id}}]" value="1">{{$attr->name }} <img style="height:40px;width:40px" class="pull-right" src="{{ asset("public/".$attr->image) }}" alt="">	
								</label>
								<label for="" class="col-md-1"></label>							
		                    	<div class="clear-fix"></div>    	                    	
		                    @endforeach
	                    </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer" style="text-align:center">
                        <button type="submit" class="btn btn-primary">Add Grocery</button>
                    </div>
                </form>
            </div>
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
<!-- /.box -->
@endsection