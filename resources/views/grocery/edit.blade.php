@extends('layout')

@section('content')
<section class="content">
    <div class="row">
        <!-- left column -->
            <!-- general form elements -->
            <div class="box box-primary">            
                <div class="box-header with-border">
                    <h3 class="box-title">Grocery Information</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                {{-- errors message --}}
            	@if (count($errors) > 0)
            	    <div class="alert alert-danger">
            	    	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
            	        <ul>
            	            @foreach ($errors->all() as $error)
            	                <li>{{ $error }}</li>
            	            @endforeach
            	        </ul>
            	    </div>
            	@endif
            	@if(session('status'))	
            		<div class="alert alert-success">
            			<p>{{ session('status') }}</p>
            		</div>	
            	@endif
                <form role="form" action="{{ url('grocerie/'.$grocery->id.'/update') }}" method="POST" enctype="multipart/form-data">
                	{{ csrf_field() }}
                	{{method_field('patch')}}
                    <div class="box-body">
        				<div class=" col-md-6">
	                        <div class="form-group">
	                            <label for="name">Name</label>
	                            <input type="text" class="form-control" id="name" name="name" value="{{$grocery->name}}">
	                        </div>
	                        <div class="form-group">
	                            <label for="logo">Logo {{$grocery->logo}}</label>
	                            <input type="file" class="form-control" id="logo" name="logo">
	                        </div>
	                        <div class="form-group">
	                            <label for="mobile">Mobile Number</label>
	                            <input type="number" class="form-control" id="mobile" name="mobile" value="{{$grocery->mobile}}">
	                        </div>

	                        <div class="form-group">
	                            <label for="rnumber">Reg Number</label>
	                            <input type="text" class="form-control" id="rnumber" name="r_number" value="{{$grocery->reg_number}}">
	                        </div>
	                        <div class="form-group">
	                            <label for="sname">Staff Name</label>
	                            <input type="text" class="form-control" id="sname" name="s_name" value="{{$grocery->staff_name}}">
	                        </div>    
	                        <div class="form-group">
	                            <label for="exampleInputEmail1">Email address</label>
	                            <input type="email" class="form-control" name="email" id="exampleInputEmail1" value="{{$grocery->email}}">
	                        </div>
	                        <div class="form-group">
	                            <label for="exampleInputPassword1">Password</label>
	                            <input type="password" class="form-control" id="exampleInputPassword1" name="password" value="{{$grocery->password}}">
	                        </div>
	                        <div class="form-group">
	                            <label for="country">Status</label>
	                            <select name="status" class="form-control" id="country">                            
									<option value="active"{{$grocery->status =='active'? 'selected = "selected"':' '}}>Active</option>
									<option value="pending"{{$grocery->status =='pending'? 'selected = "selected"':' '}}>Pending</option>
	                            </select>
	                        </div>	                       
	                        <div class="form-group">
	                            <label for="lat">Lat</label>
	                            <input type="number" class="form-control" id="lat" name="lat" value="{{$grocery->lat}}">
	                        </div>
	                    </div>
	                    <div class="col-md-6">
	                        <div class="form-group">
	                            <label for="holder">Iban Holder</label>
	                            <input type="text" name="holder" class="form-control" id="holder" value="{{$grocery->iban_holder}}" >
	                        </div>
	                        <div class="form-group">
	                            <label for="iban">Iban</label>
	                            <input type="text" name="iban" class="form-control" id="iban" value="{{$grocery->iban}}" >
	                        </div>
	                        <div class="form-group">
	                            <label for="order">Min Order</label>
	                            <input type="number" name="m_order" class="form-control" id="order" value="{{$grocery->min_order}}" >
	                        </div>
	                        <div class="form-group">
	                            <label for="description">Description</label>
	                            <input type="text" name="description" class="form-control" id="description" value="{{$grocery->description}}" >
	                        </div>
	                        <div class="form-group">
	                            <label for="city">City</label>
	                            <input type="text" name="city" class="form-control" id="city" value="{{$grocery->city}}">
	                        </div>
	                        <div class="form-group">
	                            <label for="district">District</label>
	                            <input type="text" name="district" class="form-control" id="district" value="{{$grocery->district}}" >
	                        </div>
	                        <div class="form-group">
	                            <label for="country">Country</label>
	                            <select name="country" class="form-control" id="country">
	                            	@foreach($countrys as $country)
										<option value="{{ $country->code }}" {{ ($grocery->country == $country->code) ? 'selected = "selected"':'' }}">{{ $country->name }}</option>
									@endforeach	
	                            </select>
	                        </div>
	                        <div class="form-group">
	                            <label for="address"> Address</label>
	                            <input type="text" class="form-control" id="addres" name="addres" value="{{$grocery->address_1}}">
	                        </div>

	                        <div class="form-group">
	                            <label for="lon">Lon</label>
	                            <input type="number" class="form-control" id="lon" name="lon" value="{{$grocery->lon}}">
	                        </div>                  	
	                    </div>
	                    <div class="checkbox">
		                    @foreach($attrs as $attr)
								<label class="checkbox-inline col-md-5" style="padding:18px">
								  	<input type="checkbox" id="inlineCheckbox1" name="attr[{{$attr->id}}]" value="1" {{$attrd[$attr->id] ==1?'checked="checked"':' '}}>{{ $attr->name }} <img style="height:40px;width:40px" class="pull-right" src="{{ asset("public/".$attr->image) }}" alt="">	
								</label>
								<label for="" class="col-md-1"></label>							
		                    	<div class="clear-fix"></div>
		                    @endforeach
	                    </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer" style="text-align:center">
                        <button type="submit" class="btn btn-primary">Update Grocery</button>
                    </div>
                </form>
            </div>
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
<!-- /.box -->
@endsection