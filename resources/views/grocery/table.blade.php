@extends('layout')

@section('content')
<section class="content-header">
    <h1>
        Customer Tables
        <small>advanced tables</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Customer Data Table</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>logo</th>
                                <th>Email</th>
                                <th>Number</th>
                                <th>Min Order</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($grocerys as $grocery)
                                <tr>
                                    <td>{{ $grocery->id }}</td>
                                    <td>{{ $grocery->name }}</td>
                                    <td>{{ $grocery->logo }}</td>
                                    <td>{{ $grocery->email }}</td>
                                    <td>{{ $grocery->mobile }}</td>
                                    <td>{{ $grocery->min_order }}</td>
                                    <td>
                                        <a href="{{ url('grocerie/'.$grocery->id.'/edit')}}" style="padding-right:10px"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                        <a href="" style="color:red"><i class="fa fa-trash" aria-hidden="true"></i></a>

                                        <a href="{{ url('workinhours/'.$grocery->id.'/create')}}" style="color:green; padding-left:10px"><i class="fa fa-building-o" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
@endsection