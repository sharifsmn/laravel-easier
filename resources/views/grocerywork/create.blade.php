@extends('layout')

@section('content')
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-10 col-md-offset-1">
            <!-- general form elements -->
            <div class="box box-primary">            
                <div class="box-header with-border">
                    <h3 class="box-title">Grocery Workin Information</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                {{-- errors message --}}
            	@if (count($errors) > 0)
            	    <div class="alert alert-danger">
            	        <ul>
            	            @foreach ($errors->all() as $error)
            	                <li>{{ $error }}</li>
            	            @endforeach
            	        </ul>
            	    </div>
            	@endif
                <form role="form" action="{{ url('workinhours/add') }}" method="POST" enctype="multipart/form-data">
                	{{ csrf_field() }}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name">Grocery Name</label>
                            <select name="name" class="form-control" id="name">
                                <option value="{{ $grocery->id }}">{{ $grocery->name}}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="day">Day</label>
                            <select name="day" class="form-control" id="day">
                                <option value="sat">Sat</option>
                                <option value="sun">Sun</option>
                                <option value="mon">MOn</option>
                                <option value="tw">Tw</option>
                                <option value="wd">Wd</option>
                                <option value="th">Th</option>
                                <option value="fd">Fd</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="status">Status</label>
                            <select name="status" class="form-control" id="status">
								<option value="open">Open</option>
								<option value="close">Close</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="open">Open At</label>
                            <input type="time" name="open" class="form-control" id="open" placeholder="Enter city" >
                        </div>
                        <div class="form-group">
                            <label for="close">Close At</label>
                            <input type="time" name="close" class="form-control" id="close" placeholder="Enter District" >
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Update Grocery Workin Hours</button>
                    </div>
                </form>
            </div>
        </div>    
    </div>
    <!-- /.row -->
</section>
@endsection