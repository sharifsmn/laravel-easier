<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	// return redirect(config('backpack.base.route_prefix', 'admin'));
	return redirect("admin/login");
    // return view('login');
});

// Auth::routes();

Route::group(['prefix' => 'json','middleware' => ['cors']], function () {
	Route::get('country', 'Api\CountryController@index');
	Route::resource('grocery', 'Api\GroceryController');
	Route::get('grocery/attributes/all', 'Api\GroceryAttributeController@index');
	Route::post('grocery/{grocery}/update/attrs', 'Api\GroceryController@updateAttributes');
	
	Route::resource('customer', 'Api\CustomerController');
	Route::post('customer/{customer}/update/confirm', 'Api\CustomerController@updateConfirm');
	Route::post('customer/login', 'Api\CustomerController@login');

	Route::post('register/verifycode', 'Api\VerifyController@sendVerifyCodeMobile');
	Route::post('register/grocery/verifycode', 'Api\VerifyController@sendVerifyCodeMobileGrocery');

	Route::post('status', 'Api\VerifyController@checkStatus');

	Route::any('/', 'Api\IndexController@index');
	Route::any( '{slug}', function($slug){
		$content = array('status' => "error", "error" => "Invalid method called." );
    	return response($content)->header('Content-Type', "application/json");
	})->where('slug', '.*');
});
// Customer route
Route::group(['prefix' => 'customer'], function () {
	Route::get('/', 'Customer\CustomerController@index');
	Route::get('create', 'Customer\CustomerController@create');
	Route::post('add', 'Customer\CustomerController@store');
	Route::get('{customer}/edit', 'Customer\CustomerController@edit');
	Route::patch('{customer}/update', 'Customer\CustomerController@update');
});
Route::group(['prefix' =>'grocerie'], function() {
	Route::get('/','Grocery\GroceryController@index');
	Route::get('create','Grocery\GroceryController@create');
	Route::post('add','Grocery\GroceryController@store');
	Route::get('{grocery}/edit','Grocery\GroceryController@edit');
	Route::patch('{grocery}/update','Grocery\GroceryController@update');
});
Route::group(['prefix' =>'workinhours'], function() {
	Route::get('/{grocery}/create','Grocerywork\GroceryWorkinController@create');
	Route::post('/add','Grocerywork\GroceryWorkinController@store');
});
Route::group(['prefix' => 'groceryattr'], function () {
	Route::get('/', 'Groceryattr\GroceryattrController@index');
	Route::get('create', 'Groceryattr\GroceryattrController@create');
	Route::post('add', 'Groceryattr\GroceryattrController@store');
	Route::get('{groceryattributes}/edit', 'Groceryattr\GroceryattrController@edit');
	Route::patch('{groceryattributes}/update', 'Groceryattr\GroceryattrController@update');
});

Route::group(['prefix'=>'product'],function(){
	Route::get('/','Product\ProductController@index');
	Route::get('create','Product\ProductController@create');
	Route::post('add','Product\ProductController@store');
});
Route::group(['prefix'=>'quote'],function(){
	Route::get('/','Quote\QuoteController@index');
	Route::get('create','Quote\QuoteController@create');
	Route::post('add','Quote\QuoteController@store');
});
Route::group(['prefix'=>'quoteitem'],function(){
	Route::get('/','Quoteitem\QuoteItemController@index');
	Route::get('create','Quoteitem\QuoteItemController@create');
	Route::post('add','Quoteitem\QuoteItemController@store');
});
Route::group(['prefix'=>'order'],function(){
	Route::get('/','Order\OrderController@index');
	Route::get('create','Order\OrderController@create');
	Route::post('add','Order\OrderController@store');
});
Route::group(['prefix'=>'orderitem'],function(){
	Route::get('/','Orderitem\OrderItemController@index');
	Route::get('create','Orderitem\OrderItemController@create');
	Route::post('add','Orderitem\OrderItemController@store');
});

Auth::routes();

Route::get('/home', 'Words@index');
