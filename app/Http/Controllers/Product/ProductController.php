<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Grocery;
use App\Product;

class ProductController extends Controller
{
    public function index()
    {
    	$products =Product::all();
    	return view('product.table',compact('products'));
    }
    public function create()
    {
    	$grocerys = Grocery::all();
    	return view('product.create',compact('grocerys'));
    }
    public function store(Request $request)
    {
    	$this->validate($request,[
    		'grocery' => 'required',
    		'name' => 'required',
    		'unit' => 'required',
    		'price' => 'required',
    		'status' => 'required',
		]);

		$product = new Product();
		$product->grocery_id = $request->grocery;
		$product->name = $request->name;
		$product->unit = $request->unit;
		$product->price = $request->price;
		$product->status = $request->status;
		$product->save();
		return redirect('product/');
    }
}
