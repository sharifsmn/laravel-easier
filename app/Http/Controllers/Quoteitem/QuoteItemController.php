<?php

namespace App\Http\Controllers\Quoteitem;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Quote;
use App\QuoteItem;

class QuoteItemController extends Controller
{
    public function index()
    {
    	$quoteitems = QuoteItem::all();
    	return view('quoteitem.table',compact('quoteitems'));
    }
    public function create()
    {
    	$quotes = Quote::all();
    	return view('quoteitem.create',compact('quotes'));
    }
    public function store(Request $request)
    {
    	$this->validate($request, [
    		'name' => 'required',
    		'qty' => 'required',
    		'price' => 'required',
    		'status' => 'required',
		]);

		$quote = new QuoteItem();
		$quote->quote_id = $request->quote;
		$quote->name = $request->name;
		$quote->qty = $request->qty;
		$quote->price = $request->price;
		$quote->status = $request->status;
		$quote->save();
		return redirect('quoteitem/');

    }
}
