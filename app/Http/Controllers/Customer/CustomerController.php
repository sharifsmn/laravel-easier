<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customer;

class CustomerController extends Controller
{
    public function index()
    {
        $customers = Customer::all();
    	return view('customer.table',compact('customers'));
    }

    public function create()
    {
    	$file = storage_path("app/public/countries.json");
    	$countrys = json_decode( file_get_contents($file) );

    	return view('customer.create',compact('countrys'));
    }

    public function store(Request $request)
    {

        $this->validate( $request, [
            'name' => 'required|min:4',
            'u_name' => 'required|min:4',
            'mobile' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:6',
            'status' => 'required',
            'city' => 'required',
            'district' => 'required',
            'addres' => 'required',
            'lat' => 'required',
            'lon' => 'required'

        ]);

        $customer = new Customer();
        $customer->name = $request->name;
        $customer->username = $request->u_name;
        $customer->mobile = $request->mobile;
        $customer->email = $request->email;
        $customer->city = $request->city;
        $customer->district = $request->district;
        $customer->country = $request->country;
        $customer->address_1 = $request->addres;
        $customer->lat = $request->lat;
        $customer->lon = $request->lon;
        $customer->password = $request->password;
        $customer->status = $request->status;
        $customer->save();

        return redirect('customer');
    }

    public function edit(Customer $customer)
    {
        $file = storage_path("app/public/countries.json");
        $countrys = json_decode( file_get_contents($file) );        
        return view('customer.edit',compact('customer','countrys'));
    }

    public function update(Request $request, Customer $customer)
    {
        $customer->name = $request->name;
        $customer->mobile = $request->mobile;
        $customer->email = $request->email;
        $customer->city = $request->city;
        $customer->district = $request->district;
        $customer->country = $request->country;
        $customer->address_1 = $request->addres;
        $customer->lat = $request->lat;
        $customer->lon = $request->lon;
        $customer->password = $request->password;
        $customer->status = $request->status;
        $customer->update();

        $request->session()->flash('activity_msg_success','Hi Admin! Customer updated successfully');        
        return redirect('customer/'.$customer->id.'/edit');   
    }
}
