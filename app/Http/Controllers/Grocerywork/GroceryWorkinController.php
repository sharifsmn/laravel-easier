<?php

namespace App\Http\Controllers\Grocerywork;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Grocery;
use App\GroceryWorkingHours;

class GroceryWorkinController extends Controller
{
    public function create(Grocery $grocery)
    {
    	return view('grocerywork.create',compact('grocery'));
    }
    public function store(Request $request)
    {
    	$this->validate($request, [
    		'name' => 'required',
    		'day' => 'required',
    		'status' => 'required',
    		'open' => 'required',
    		'close' => 'required'
		]);

		$workin = new GroceryWorkingHours();
		$workin->grocery_id = $request->name;
		$workin->day = $request->day;
		$workin->status = $request->status;
		$workin->open_at = $request->open;
		$workin->close_at = $request->close;
		$workin->save();
		return redirect('grocerie/');
    }
}
