<?php

namespace App\Http\Controllers\Orderitem;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderItem;

class OrderItemController extends Controller
{
    public function index()
    {
    	$orderitems =OrderItem::all();
    	return view('orderitem.table',compact('orderitems'));
    }
    public function create()
    {
    	$orders = Order::all();
    	return view('orderitem.create',compact('orders'));
    }
    public function store(Request $request)
    {
    	$this->validate($request,[
    		'name' => 'required',
    		'qty' => 'required',
    		'price' => 'required',
		]);

		$orderitem = new OrderItem();
		$orderitem->order_id = $request->order;
		$orderitem->name = $request->name;
		$orderitem->qty = $request->qty;
		$orderitem->price = $request->price;
		$orderitem->save();
		return redirect('orderitem/');
    }
}
