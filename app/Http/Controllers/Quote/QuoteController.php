<?php

namespace App\Http\Controllers\Quote;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Grocery;
use App\Customer;
use App\Quote;

class QuoteController extends Controller
{
    public function index()
    {
    	$quotes = Quote::all();
    	return view('quote.table',compact('quotes'));
    }
    public function create()
    {
    	$customers = Customer::all();
    	$grocerys = Grocery::all();
    	return view('quote.create',compact('customers','grocerys'));
    }

    public function store(Request $request)
    {
    	$this->validate($request,[
    		'customer' => 'required',
    		'grocery' => 'required',
    		'lat' => 'required',
    		'lon' => 'required',
    		'status' => 'required',
    		'comment' => 'required'
		]);

		$quote = new Quote();
		$quote->customer_id = $request->customer;
		$quote->grocery_id = $request->grocery;
		$quote->lat = $request->lat;
		$quote->lon = $request->lon;
		$quote->status = $request->status;
		$quote->comment = $request->comment;
		$quote->save();
		return redirect('quote/');
    }
}
