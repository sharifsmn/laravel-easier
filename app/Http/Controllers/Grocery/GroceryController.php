<?php

namespace App\Http\Controllers\Grocery;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\GroceryAttributes;
use App\Grocery;
use App\GroceryAttributesValue;
use Storage;

class GroceryController extends Controller
{
    public function index()
    {
    	$grocerys = Grocery::all();
    	return view('grocery.table',compact('grocerys'));
    }
    public function create()
    {
    	$file = storage_path("app/public/countries.json");
    	$countrys = json_decode( file_get_contents($file) );
    	$attrs = GroceryAttributes::all();

    	return view('grocery.create',compact('countrys','attrs'));
    }

    public function store(Request $request)
    {
    	$this->validate( $request, [
			'name' => 'required',    		
			'logo' => 'required|mimes:jpeg,jpg,png | max:2028',    		
			'mobile' => 'required',    		
			'r_number' => 'required',    		
			's_name' => 'required',    		
			'email' => 'required',    		
			'password' => 'required',    		
			'lat' => 'required',    		
			'holder' => 'required',    		
			'iban' => 'required',    		
			'm_order' => 'required',    		
			'description' => 'required',    		
			'city' => 'required',    		
			'district' => 'required',    		
			'addres' => 'required',    		
			'lon' => 'required',    		
		]);

		$logo =$request->logo;
		$fillable =$logo->getClientOriginalName();
		Storage::put('upload/logo/'.$fillable, file_get_contents($request->file('logo')->getRealPath()));

		$attr_ids = GroceryAttributes::all();
		$grocery = new Grocery;

		$grocery->name = $request->name;
		$grocery->logo = $fillable;
		$grocery->mobile = $request->mobile;
		$grocery->reg_number = $request->r_number;
		$grocery->staff_name = $request->s_name;
		$grocery->iban_holder = $request->holder;
		$grocery->iban = $request->iban;
		$grocery->min_order = $request->m_order;
		$grocery->description = $request->description;
		$grocery->city = $request->city;
		$grocery->district = $request->district;
		// $grocery->country = $request->country;
		$grocery->address_1 = $request->addres;
		$grocery->lat = $request->lat;
		$grocery->lon = $request->lon;
		$grocery->email = $request->email;
		$grocery->password = md5($request->password);
		$grocery->status = $request->status;
		$grocery->save();

		$attrs = $request->attr;

		foreach ($attr_ids as $attr) {
			$value =0;
			$attrv = new GroceryAttributesValue;

			$attrv->grocery_id =$grocery->id;
			$attrv->attribute_id =$attr->id;
			if(isset($attrs[$attr->id]) ) $value = $attrs[$attr->id];
			$attrv->value = $value;
			$attrv->save();
		}

		return redirect('grocerie/');
    }
    public function edit(Grocery $grocery)
    {

    	$file = storage_path("app/public/countries.json");
    	$countrys = json_decode( file_get_contents($file) );
    	$attrs = GroceryAttributes::all();
    	$attrd =[];
    	$attrvs =$grocery->attrValue;
    	foreach ($attrs as $attr) {
    		$attrd[$attr->id] = 0;
    	}
    	foreach ($attrvs as $attrv) {
    		$attrd[$attrv->attribute_id] = $attrv->value;    		
    	}
    	return view('grocery.edit',compact('grocery','countrys','attrs','attrd'));
    }
    public function update(Request $request, Grocery $grocery, GroceryAttributesValue $groceryAttributesValue)
    {
    	$this->validate( $request, [
			'name' => 'required',    		
			'logo' => 'required|mimes:jpeg,jpg,png | max:2028',    		
			'mobile' => 'required',    		
			'r_number' => 'required',    		
			's_name' => 'required',    		
			'email' => 'required',    		
			'password' => 'required',    		
			'lat' => 'required',    		
			'holder' => 'required',    		
			'iban' => 'required',    		
			'm_order' => 'required',    		
			'description' => 'required',    		
			'city' => 'required',    		
			'district' => 'required',    		
			'addres' => 'required',    		
			'lon' => 'required',    		
		]);

		$logo =$request->logo;
		$fillable =$logo->getClientOriginalName();
		Storage::put('upload/logo/'.$fillable, file_get_contents($request->file('logo')->getRealPath()));

		$attr_ids = GroceryAttributes::all();

		$grocery->name = $request->name;
		$grocery->logo = $fillable;
		$grocery->mobile = $request->mobile;
		$grocery->reg_number = $request->r_number;
		$grocery->staff_name = $request->s_name;
		$grocery->iban_holder = $request->holder;
		$grocery->iban = $request->iban;
		$grocery->min_order = $request->m_order;
		$grocery->description = $request->description;
		$grocery->city = $request->city;
		$grocery->district = $request->district;
		// $grocery->country = $request->country;
		$grocery->address_1 = $request->addres;
		$grocery->lat = $request->lat;
		$grocery->lon = $request->lon;
		$grocery->email = $request->email;
		$grocery->password = md5($request->password);
		$grocery->status = $request->status;
		$grocery->update();

		$data =[];
		$attrvs =$grocery->attrValue;
		foreach ($attrvs as $item) {
			$data[$item->attribute_id] = $item;
		}

		$attrs = $request->attr;
		foreach ($attr_ids as $attr) {
			$value =0;
			if (isset($attrs[$attr->id]) && array_key_exists($attr->id, $data)) {
				// $attrv = new GroceryAttributesValue();
				$attrv = $data[$attr->id];
				$attrv->attribute_id =$attr->id;
				if(isset($attrs[$attr->id]) ) $value = $attrs[$attr->id];
				$attrv->value = $value;
				$attrv->update();
			}else if (!isset($attrs[$attr->id]) && array_key_exists($attr->id, $data)) {
				$attrv = $data[$attr->id];
				$attrv->attribute_id =$attr->id;
				$attrv->value = 0;
				$attrv->update();
			}else{
				$attrv = new GroceryAttributesValue();
				$attrv->grocery_id =$grocery->id;
				$attrv->attribute_id =$attr->id;
				if(isset($attrs[$attr->id]) ) $value = $attrs[$attr->id];
				$attrv->value = $value;
				$attrv->save();
			}
		}
		$request->session()->flash('status','Hi Admin. Grocery Successfully Updated!!');
		return redirect('grocerie/'.$grocery->id.'/edit');
    }
}
