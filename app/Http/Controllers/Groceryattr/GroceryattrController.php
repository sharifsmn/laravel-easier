<?php

namespace App\Http\Controllers\Groceryattr;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\GroceryAttributes;
use Storage;
use Session;

class GroceryattrController extends Controller
{
    public function index()
    {
    	$grocerys =GroceryAttributes::paginate(10);
    	return view('groceryattr.table',compact('grocerys'));
    }

    public function create()
    {
    	return view('groceryattr.create');
    }

    public function store(Request $request)
    {
    	$this->validate($request,[
    		'code' => 'required|unique:grocery_attributes,code',
    		'name' => 'required',
    		'image' => 'required|mimes:jpeg,jpg,png | max:2028',
    		'open' => 'required',
    		'close' => 'required',
    		'order' => 'required',
		]);
		$image =$request->image;
		$fillable =$image->getClientOriginalName();
		Storage::put('upload/image/'.$fillable, file_get_contents($request->file('image')->getRealPath()));

        $attr = new GroceryAttributes();
        $attr->code = $request->code;
        $attr->name = $request->name;
        $attr->image = $fillable;
        $attr->type = $request->type;
        $attr->open_at = $request->open;
        $attr->close_at = $request->close;
        $attr->order = $request->order;
        $attr->save();

        return redirect('groceryattr/');
    }
    public function edit(GroceryAttributes $groceryattributes)
    {
        return view('groceryattr.edit',compact('groceryattributes'));
    }

    public function update(Request $request, GroceryAttributes $groceryattributes)
    {
        $this->validate($request,[
            'code' => 'required|unique:grocery_attributes,code',
            'name' => 'required',
            'image' => 'mimes:jpeg,jpg,png | max:2028',
            'open' => 'required',
            'close' => 'required',
            'order' => 'required',
        ]);
        $image =$request->image;
        $fillable =$image->getClientOriginalName();
        Storage::put('upload/image/'.$fillable, file_get_contents($request->file('image')->getRealPath()));

        $groceryattributes->code = $request->code;
        $groceryattributes->name = $request->name;
        $groceryattributes->image = $fillable;
        $groceryattributes->type = $request->type;
        $groceryattributes->open_at = $request->open;
        $groceryattributes->close_at = $request->close;
        $groceryattributes->order = $request->order;
        $groceryattributes->update(); 
        $request->session()->flash('status', 'Data Update Successfully completesuccessful!');
        return redirect('groceryattr/'.$groceryattributes->id.'/edit');          
    }
}
