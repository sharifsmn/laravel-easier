<?php

namespace App\Http\Controllers;

use App\Product;
use App\Words as WordsModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use App\Helpers\CJ;

class Words extends Controller
{
	public function __construct() {
        $this->middleware('admin');
    }

    public function index(){
        return view('words.index', [
            'wordss' => WordsModel::All()
        ]);
    }
    
    public function add() {
        return view('words.add', [
        	'products' => Product::All(),
        ]);
    }

    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'bail|required|max:255',
            'product_id' => 'required',
        ]);

        $token = Str::random(rand(25,50));
        $data = $request->except(['_token']);
        $data["ctoken"] = $token;
        $data["url"] = url('click/'.$token);

        $item = WordsModel::create($data);
        return Redirect::to('admin/words/'.$item->id);
    }

    public function edit($id) {
        $item = WordsModel::findOrFail($id);
        return view('words.store', [
        	'item' => $item,
        	'products' => Product::All(),
        	'charities' => CharityModel::All(),
        ]);
    }
    
    public function update($id, Request $request) {
        $this->validate($request, [
            'name' => 'bail|required|max:255',
            'product_id' => 'required',
        ]);

        $data = $request->except(['_token']);

        $item = WordsModel::findOrFail($id);
        
        $item->name = $data["name"];
        $item->product_id = $data["product_id"];
        $item->description = $data["description"];
        $item->save();

        $request->session()->flash('activity_msg_success','Words updated successfully');
        return Redirect::to('admin/words/'.$item->id);
    }
    
    public function delete($id, Request $request) {

    }   
}
