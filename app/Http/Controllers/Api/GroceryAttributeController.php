<?php

namespace App\Http\Controllers\Api;

use App\GroceryAttributes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GroceryAttributeController extends Controller
{
    public function index(Request $request){
    	$token_new = csrf_token();
        $token_old = $request->header('X-CSRF-TOKEN');
        // $data = ["told"=> $token_old, "tnew"=>$token_new];
        
    	$content = array( 'status' => "error", "statuscode" => 403, "error" => "Unable to process request." );
    	if($token_old && hash_equals($token_new, $token_old)){
	    	$data = GroceryAttributes::all();
	    	$token = $request->get("token");
	    	$content = array( 'status' => "ok", "statuscode" => 200, "data"=> $data );

	    	return response($content)
	            ->header('X-CSRF-TOKEN', $token_new)
	            ->header('Content-Type', "application/json");
    	}

    	return response($content)
            ->header('Content-Type', "application/json");
    }
}
