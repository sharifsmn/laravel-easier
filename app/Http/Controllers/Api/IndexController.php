<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function __construct() {
        // $this->middleware('admin');
    }

    public function index($uri=null){
    	$content = array('status' => "error", "error" => "Invalid method called." );
    	return response($content)->header('Content-Type', "application/json");
    }
}
