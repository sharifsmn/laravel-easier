<?php

namespace App\Http\Controllers\Api;

use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\MobileVerificationCodes;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        echo "Customer API";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $token_new = csrf_token();
        $token_old = $request->header('X-CSRF-TOKEN');
        $content = array( 'status' => "error", "error" => "Unable to process request." );

        if(hash_equals($token_new, $token_old)){
            $validator = Validator::make($request->all(), [
                'code' => 'required|max:5',
                'number' => 'required|max:15',
                'verfication_code' => 'required|min:6',
            ]);

            if ($validator->fails()) {
                $content["error"] = "Country code, Mobile number and verificaion code is required.";
            }else{
                $code = $request->input("code");
                $number = $request->input("number");
                $varify_code = $request->input("verfication_code");
                $mobile_number = $code.$number;

                $codeItem = MobileVerificationCodes::where( [["mobile", $mobile_number],["scope", "registration"]] )->first();

                if( $codeItem && $codeItem->code == $varify_code ){

                    $codeItem->delete();
                    
                    $customer = new Customer();
                    $customer->username = $mobile_number;
                    $customer->mobile = $mobile_number;
                    $customer->country = $code;
                    $customer->password = '';
                    $customer->save();

                    $customer = Customer::find($customer->id);

                    $content["status"] = "ok";
                    $content["data"] = $customer;
                    unset($content["error"]);
                }else{
                    $content["error"] = "Verificaion code does not match.";
                }
            }

        }
        return response($content)
            ->header('X-CSRF-TOKEN', $token_new)
            ->header('Content-Type', "application/json");
    }

    public function updateConfirm(Request $request, Customer $customer)
    {
        $token_new = csrf_token();
        $token_old = $request->header('X-CSRF-TOKEN');
        $content = array( 'status' => "error", "error" => "Unable to process request." );

        if(hash_equals($token_new, $token_old)){
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                // 'email' => 'required',
                'password' => 'required|min:6',
            ]);

            if ($validator->fails()) {
                $content["error"] = "All fields are not filled properly.";
            }else{
                $name = $request->input("name");
                $email = $request->input("email");
                $password = $request->input("password");

                // $customer = new Customer();
                if( $email ) $customer->email = $email;
                $customer->name = $name;
                $customer->password = md5($password);
                $customer->status = "active";
                $customer->save();

                // $customer = Customer::find($customer->id);
                // $content["error"] = "Verificaion code does not match.";

                $content["status"] = "ok";
                $content["data"] = $customer;
                unset($content["error"]);
            }
        }
        return response($content)
            ->header('X-CSRF-TOKEN', $token_new)
            ->header('Content-Type', "application/json");
    }
    
    public function login(Request $request)
    {
        $token_new = csrf_token();
        $token_old = $request->header('X-CSRF-TOKEN');
        $content = array( 'status' => "error", "error" => "Unable to process request." );

        if(hash_equals($token_new, $token_old)){
            $validator = Validator::make($request->all(), [
                'username' => 'required',
                'pass' => 'required|min:6',
            ]);

            if ($validator->fails()) {
                $content["error"] = "All fields are not filled properly.";
            }else{
                $username = $request->input("username");
                $password = $request->input("pass");
                $password_enc = md5($password);

                $customer = Customer::where( [["status", "active"],["username", $username],["password", $password_enc]] )->first();
                if($customer){
                    $content["status"] = "ok";
                    $content["data"] = $customer;
                    unset($content["error"]);
                }else{
                    $content["error"] = "Credentials does not match.";
                }
            }
        }
        return response($content)
            ->header('X-CSRF-TOKEN', $token_new)
            ->header('Content-Type', "application/json");
    }




    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        //
    }
}
