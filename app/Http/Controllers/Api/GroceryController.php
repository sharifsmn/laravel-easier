<?php

namespace App\Http\Controllers\Api;

use App\Grocery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\MobileVerificationCodes;

class GroceryController extends Controller
{

	public function index()
    {
        echo "Grocery API";
    }
    
	public function store(Request $request)
    {
        $token_new = csrf_token();
        $token_old = $request->header('X-CSRF-TOKEN');
        $content = array( 'status' => "error", "error" => "Unable to process request." );

        if(hash_equals($token_new, $token_old)){

            $validator = Validator::make($request->all(), [
                "mobile" => 'required|min:6',
                "name" => 'required',
                "reg_number" => 'required',
                "iban_holder" => 'required',
                "staff_name" => 'required',
                "iban" => 'required',
                "min_order" => 'required',
                "description" => 'required',
                "sms_code" => 'required',
                "city" => 'required',
                "district" => 'required',
                "address" => 'required',
                // "working_hours" => 'required',
                "accept_terms_conds" => 'required',
                "lat" => 'required',
                "lon" => 'required',
                "email" => 'required',
                "password" => 'required',
            ]);

            if ($validator->fails()) {
                // $content["error"] = "Pls fill all the fields.";
                $content["error"] = $validator->errors();
            }else{
				$mobile = $request->input("mobile");
				$logo = $request->input("logo");
				$name = $request->input("name");
				$reg_number = $request->input("reg_number");
				$iban_holder = $request->input("iban_holder");
				$staff_name = $request->input("staff_name");
				$iban = $request->input("iban");
				$min_order = $request->input("min_order");
				$description = $request->input("description");
				$sms_code = $request->input("sms_code");
				$city = $request->input("city");
				$district = $request->input("district");
				$address = $request->input("address");
				$working_hours = $request->input("working_hours");
				$accept_terms_conds = $request->input("accept_terms_conds");
				$lat = $request->input("lat");
				$lon = $request->input("lon");
				$email = $request->input("email");
				$password = $request->input("password");

                $codeItem = MobileVerificationCodes::where( [["mobile", $mobile],["scope", "registration_grocery"]] )->first();

                if( $codeItem && $codeItem->code == $sms_code ){

                    $codeItem->delete();
                    
                    $grocery = new Grocery();
					$grocery->name = $name;
					$grocery->logo = $logo;
					$grocery->mobile = $mobile;
					$grocery->reg_number = $reg_number;
					$grocery->staff_name = $staff_name;
					$grocery->iban_holder = $iban_holder;
					$grocery->iban = $iban;
					$grocery->min_order = $min_order;
					$grocery->description = $description;
					$grocery->city = $city;
					$grocery->district = $district;
					$grocery->address_1 = $address;
					$grocery->lat = $lat;
					$grocery->lon = $lon;
					$grocery->password = md5($password);
					$grocery->status = "active";
                    $grocery->save();

                    $grocery = Grocery::find($grocery->id);

                    $content["status"] = "ok";
                    $content["data"] = $grocery;
                    unset($content["error"]);
                }else{
                    $content["error"] = "Verificaion code does not match.";
                }
            }

        }
        return response($content)
            ->header('X-CSRF-TOKEN', $token_new)
            ->header('Content-Type', "application/json");
    }

    public function updateAttributes(Request $request, Grocery $grocery)
    {
        $token_new = csrf_token();
        $token_old = $request->header('X-CSRF-TOKEN');
        $content = array( 'status' => "error", "error" => "Unable to process request." );

        if(hash_equals($token_new, $token_old)){
        	$attrs = $request->all();

            if(count($attrs) == 0) {
                $content["error"] = "No data found";
            }else{
                // $customer = Customer::find($customer->id);
                // $content["error"] = "Verificaion code does not match.";

                $content["status"] = "ok";
                $content["data"] = $attrs;
                unset($content["error"]);
            }
        }
        return response($content)
            ->header('X-CSRF-TOKEN', $token_new)
            ->header('Content-Type', "application/json");
    }


}
