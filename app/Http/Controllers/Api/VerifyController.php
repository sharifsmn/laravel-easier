<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\MobileVerificationCodes;
use App\Customer;
use App\Grocery;
use Aloha\Twilio\Support\Laravel\Facade as Twilio;

class VerifyController extends Controller
{
    
	public function __construct() {
        // $this->middleware('cors');
    }

    public function index($uri=null){
    	$content = array('status' => "error", "error" => "Invalid method called." );
    	return response($content)
	    	->header('X-CSRF-TOKEN', csrf_token())
	    	->header('Content-Type', "application/json");
    }

    public function sendVerifyCodeMobileGrocery(Request $request){
    	$token_new = csrf_token();
    	$token_old = $request->header('X-CSRF-TOKEN');
    	$content = array( 'status' => "error", "error" => "Unable to process request." );

    	if(hash_equals($token_new, $token_old)){
	    	$validator = Validator::make($request->all(), [
	            'number' => 'required|max:15',
	        ]);

	        if ($validator->fails()) {
	        	$content["error"] = "Mobile number is required.";
	    		$content["data"] = false;
	        }else{
	    		$mobile_number = $request->input("number");

	    		$grocery = Grocery::where( [["mobile", $mobile_number]] )->first();

	    		if($grocery){
	    			$content["error"] = "This Mobile number is already registered.";
	    			$content["data"] = false;
	    		}else{
		    		MobileVerificationCodes::where( [["mobile", $mobile_number],["scope", "registration_grocery"]] )->delete();
		    		
		    		$x = 6;
					$min = pow(10, $x - 1);
					$max = pow(10, $x) - 1;
					$varify_code = rand($min, $max);
					$varify_code = 123456;
					$message = "Easier App.\n Verification Code: $varify_code";

					try{
						// Twilio::message("+".$mobile_number, $message);	

			    		$codeItem = new MobileVerificationCodes();
			    		$codeItem->mobile = $mobile_number;
			    		$codeItem->scope = "registration_grocery";
			    		$codeItem->status = "active";
			    		$codeItem->code = $varify_code;
			    		$codeItem->save();

			    		// $content["verifyCode"] = $varify_code;
			    		$content["status"] = "ok";
			    		$content["data"] = true;
			    		unset($content["error"]);

					}catch(\Exception $e){
						$content["error"] = $e->getMessage();
	    				$content["data"] = false;
					}

					
	    		}
	        }

    	}
    	return response($content)
            ->header('X-CSRF-TOKEN', $token_new)
            ->header('Content-Type', "application/json");
    }

    public function sendVerifyCodeMobile(Request $request){
    	$token_new = csrf_token();
    	$token_old = $request->header('X-CSRF-TOKEN');
    	$content = array( 'status' => "error", "error" => "Unable to process request." );

    	if(hash_equals($token_new, $token_old)){
	    	$validator = Validator::make($request->all(), [
	            'code' => 'required|max:5',
	            'number' => 'required|max:15',
	        ]);

	        if ($validator->fails()) {
	        	$content["error"] = "Country code and Mobile number is required.";
	    		$content["data"] = false;
	        }else{
	    		$code = $request->input("code");
	    		$number = $request->input("number");
	    		$mobile_number = $code.$number;

	    		$customer = Customer::where( [["mobile", $mobile_number]] )->first();

	    		if($customer){
	    			$content["error"] = "This Mobile number is already registered.";
	    			$content["data"] = false;
	    		}else{
		    		MobileVerificationCodes::where( [["mobile", $mobile_number],["scope", "registration"]] )->delete();
		    		
		    		$x = 6;
					$min = pow(10, $x - 1);
					$max = pow(10, $x) - 1;
					$varify_code = rand($min, $max);
					// $varify_code = 123456;
					$message = "Easier App.\n Verification Code: $varify_code";

					try{
						Twilio::message("+".$mobile_number, $message);	

			    		$codeItem = new MobileVerificationCodes();
			    		$codeItem->mobile = $mobile_number;
			    		$codeItem->scope = "registration";
			    		$codeItem->status = "active";
			    		$codeItem->code = $varify_code;
			    		$codeItem->save();

			    		// $content["verifyCode"] = $varify_code;
			    		$content["status"] = "ok";
			    		$content["data"] = true;
			    		unset($content["error"]);

					}catch(\Exception $e){
						$content["error"] = $e->getMessage();
	    				$content["data"] = false;
					}

					
	    		}
	        }

    	}
    	return response($content)
            ->header('X-CSRF-TOKEN', $token_new)
            ->header('Content-Type', "application/json");
    }


    public function checkStatus(Request $request){
    	$token_new = csrf_token();
    	$content = array( 'status' => "ok" );

    	return response($content)
            ->header('X-CSRF-TOKEN', $token_new)
            ->header('Content-Type', "application/json");
    }


}
