<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;

class CountryController extends Controller
{
    public function __construct() {
        // $this->middleware('cors');
    }

    public function index(){
    	$file = storage_path("app/public/countries.json");
    	$data = json_decode( file_get_contents($file) );
    	$content = array('status' => "ok", "data" => $data );
    	return response($content)
            ->header('X-CSRF-TOKEN', csrf_token())
            ->header('Content-Type', "application/json");
    }
}
