<?php

namespace App\Http\Controllers\Order;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customer;
use App\Grocery;
use App\Quote;
use App\Order;

class OrderController extends Controller
{
    public function index()
    {
    	$orders =Order::all();
    	return view('order.table',compact('orders'));
    }
    public function create()
    {
    	$customers =Customer::all();
    	$grocerys =Grocery::all();
    	$quotes =Quote::all();
    	return view('order.create',compact('customers','grocerys','quotes'));
    }
    public function store(Request $request)
    {
    	$this->validate($request,[
    		'customer' => 'required',
    		'grocery' => 'required',
    		'quote' => 'required',
    		'status' => 'required',
    		'comment' => 'required',

		]);

		$order = new Order();
		$order->customer_id = $request->customer;
		$order->grocery_id = $request->grocery;
		$order->quote_id = $request->quote;
		$order->status = $request->status;
		$order->comment = $request->comment;
		$order->save();
		return redirect('order/');
    }
}
