<?php

namespace App\Facades;
use Illuminate\Support\Facades\Facade;


class GroceryAuth extends Facade
{
	protected static function getFacadeAccessor() { return 'auth.driver_grocery'; }
}