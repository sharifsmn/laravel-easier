<?php

namespace App\Facades;
use Illuminate\Support\Facades\Facade;


class CustomerAuth extends Facade
{
	
	protected static function getFacadeAccessor() { return 'auth.driver_customer'; }
}