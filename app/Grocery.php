<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Grocery extends Authenticatable
{
    use HasApiTokens, Notifiable;

    public function attrValue(){
    	return $this->hasMany('App\GroceryAttributesValue');
    }
}
